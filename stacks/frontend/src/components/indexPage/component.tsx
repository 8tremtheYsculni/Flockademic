require('./styles.scss');

import * as React from 'react';
import { OutboundLink } from 'react-ga';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { AppState } from '../../ducks/app';
import { ActivityTicker } from '../activityTicker/component';
import { OrcidButton } from '../orcidButton/component';
import { PageMetadata } from '../pageMetadata/component';
import { ProfileSearch } from '../profileSearch/component';

// tslint:disable-next-line:no-submodule-imports
const rssIcon = require('material-design-icons/communication/svg/production/ic_rss_feed_48px.svg');
// tslint:disable-next-line:no-submodule-imports
const emailIcon = require('material-design-icons/communication/svg/production/ic_email_48px.svg');

const cardImage = {
  alt: 'More impact with your research — claim your academic homepage.',
  url: require('../../../../../brand/Card.png'),
};
const bgUrl = require('../../img/hero_bg.jpg');
const orcidIcon = require('../../img/orcid_logo.svg');
const openAccessLogo = require('../../img/open_access_logo.svg');
const profileScreenshot = require('../../img/screenshot_profile.png');
const articlesScreenshot = require('../../img/screenshot_profile_articles.png');

const heroStyles = {
  backgroundImage: `linear-gradient(to right, rgba(0,0,0,.5) 50%, rgba(0,0,0,.2)), url(${bgUrl})`,
};

export const IndexPage = () => (
  <div>
    <header className="hero is-large is-primary is-bold indexHero" style={heroStyles}>
      <div className="hero-body">
        <div className="container">
          <h1 className="title has-text-weight-normal">Cultivate your research audience</h1>
          <PageMetadata url="https://Flockademic.com" image={cardImage}/>
          <div>
            <ClaimButton
              className="button is-large is-primary"
              title="Build your profile"
              loggedInLabel="Claim your academic homepage"
            >
              Sign in with ORCID
            </ClaimButton>
          </div>
        </div>
      </div>
    </header>
    <section className="hero is-medium">
      <div className="hero-body">
        <div className="container">
          <div className="columns">
            <div className="column is-half-widescreen">
              <ActivityTicker title={<h2 className="title">Recently shared research</h2>}/>
            </div>
            <div className="column is-offset-1-widescreen">
              <div className="box">
                <h2 className="title">Find a researcher</h2>
                <ProfileSearch/>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section className="hero is-medium is-light">
      <div className="hero-body">
        <div className="container">
          <div className="walkthrough">
            <div className="walkthrough__description">
              <div>
                <h2 className="title">
                  Your academic homepage, ready to go
                </h2>
                <p className="content">
                  {/* tslint:disable-next-line:max-line-length */}
                  Your affliations, biography, website links&hellip; All imported automatically, waiting for you. Setting up an academic homepage has never been easier!
                </p>
                <p className="content">
                  <ClaimButton
                    title="Sign in with ORCID to populate your Flockademic profile"
                    className="button is-primary is-medium"
                    loggedInLabel="Claim your academic homepage"
                  >
                    <span>Sign in to claim your homepage</span>
                  </ClaimButton>
                </p>
              </div>
            </div>
            <div className="walkthrough__graphic is-hidden-mobile">
              <img src={profileScreenshot} className="image" alt=""/>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section className="hero is-medium">
      <div className="hero-body">
        <div className="container">
          <div className="walkthrough is-reversed">
            <div className="walkthrough__description">
              <div>
                <h2 className="title">
                  All your work in a single place
                </h2>
                <p className="content">
                  {/* tslint:disable-next-line:max-line-length */}
                  Let your publications support each other. Your Open Access publications are automatically added to your academic homepage on Flockademic. Publication not listed? It's just a few simple steps to add your preprints.
                </p>
                <p className="content">
                  <ClaimButton
                    title="Submit new manuscript"
                    className="button is-primary is-medium"
                    loggedInLabel="Import your publications"
                  >
                    <span>Sign in to import your publications</span>
                  </ClaimButton>
                </p>
              </div>
            </div>
            <div className="walkthrough__graphic">
              <img src={articlesScreenshot} className="image" alt=""/>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section className="hero is-medium is-light">
      <div className="hero-body">
        <div className="container">
          <div className="walkthrough">
            <div className="walkthrough__description">
              <div>
                <h2 className="title">
                  Simply good research!
                </h2>
                <p className="content">
                  {/* tslint:disable-next-line:max-line-length */}
                  Open research is simply good research! Contribute to a world in which publicly funded research is also publicly available.
                </p>
                <p className="content">
                  Ah, and did you know that Flockademic is a non-profit?
                </p>
                <p className="content">
                  <ClaimButton
                    title="Submit new manuscript"
                    className="button is-primary is-medium"
                  >
                    <span>Open up your research now</span>
                  </ClaimButton>
                </p>
              </div>
            </div>
            <div className="walkthrough__graphic is-hidden-mobile">
              <img src={openAccessLogo} className="image" alt=""/>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section className="section">
      <div className="container">
        <h2 className="title is-size-3">
          Latest blog posts
        </h2>
        <ul>
          <li className="media">
            <div className="media-content">
              <div className="content">
                <h3 className="title is-size-5">
                  <OutboundLink
                    eventLabel="blog-article"
                    title="Launching Unpaywall integration: automatic import of your academic research."
                    /* tslint:disable-next-line:max-line-length */
                    to="https://medium.com/flockademic/flockademic-unpaywall-8de219e78ff0"
                  >
                    Flockademic ♥ Unpaywall
                  </OutboundLink>
                </h3>
                <p>
                  {/* tslint:disable-next-line:max-line-length */}
                  Your academic research is now automatically imported into Flockademic, thanks to a new integration with Unpaywall.
                </p>
              </div>
            </div>
          </li>
          <li className="media">
            <div className="media-content">
              <div className="content">
                <h3 className="title is-size-5">
                  <OutboundLink
                    eventLabel="blog-article"
                    title="To fix scholarly publishing, decouple credentialing from publishing"
                    // tslint:disable-next-line:max-line-length
                    to="https://medium.com/flockademic/to-fix-scholarly-publishing-decouple-credentialing-from-publishing-29b211f49acc"
                  >
                    To fix scholarly publishing, decouple credentialing from publishing
                  </OutboundLink>
                </h3>
                <p>
                  {/* tslint:disable-next-line:max-line-length */}
                  Why do researchers keep submitting their work to expensive and/or closed-access journals? And how can we change that?
                </p>
              </div>
            </div>
          </li>
          <li className="media">
            <div className="media-content">
              <div className="content">
                <h3 className="title is-size-5">
                  <OutboundLink
                    eventLabel="blog-article"
                    title="The holy grail of Open Access: sharing that benefits authors"
                    // tslint:disable-next-line:max-line-length
                    to="https://medium.com/flockademic/the-holy-grail-in-open-access-sharing-that-benefits-authors-b685ff2c6300"
                  >
                    The holy grail of Open Access: sharing that benefits authors
                  </OutboundLink>
                </h3>
                <p>
                {/* tslint:disable-next-line:max-line-length */}
                As a researcher, you are often urged to make your work openly accessible. And sure, that’s a laudable goal, but… What’s in it for you?
                </p>
              </div>
            </div>
          </li>
          <li className="media">
            <p className="media-content">
              <small>
                <OutboundLink
                  eventLabel="blog-archive"
                  title="Back archive of Flockademic blog posts"
                  to="https://medium.com/Flockademic"
                >
                  View all posts
                </OutboundLink>
              </small>
            </p>
            <div className="media-right field is-grouped">
              <div className="control">
                <OutboundLink
                  to="https://tinyletter.com/Flockademic"
                  title="Subscribe to the newsletter"
                  eventLabel="newsletter"
                  className="button"
                >
                  <span className="icon">
                    <img src={emailIcon} alt="Subscribe to newsletter"/>
                  </span>
                </OutboundLink>
              </div>
              <div className="control">
                <OutboundLink
                  to="https://medium.com/feed/flockademic"
                  rel="alternate"
                  type="application/rss+xml"
                  title="Subscribe to the feed"
                  eventLabel="blog-feed"
                  className="button"
                >
                  <span className="icon">
                    <img src={rssIcon} alt="Subscribe to feed"/>
                  </span>
                </OutboundLink>
              </div>
              <Helmet>
                <link rel="alternate" type="application/rss+xml" href="https://medium.com/feed/flockademic" />
              </Helmet>
            </div>
          </li>
        </ul>
      </div>
    </section>
  </div>
);

interface ClaimButtonOwnProps { className?: string; title?: string; loggedInLabel?: string; }
interface ClaimButtonReduxProps { orcid?: string; }
type ClaimButtonProps = ClaimButtonReduxProps & ClaimButtonOwnProps;

// Since this is internal to the index page only and is relatively trivial,
// I don't feel like exporting it just for tests:
/* istanbul ignore next: */
const BareClaimButton: React.SFC<ClaimButtonProps> = (props) => {
  const loggedInButton = (
    <Link
      to="/profile"
      title={props.title || 'Build your profile'}
      className={props.className || 'button'}
    >
      {props.loggedInLabel || props.children}
    </Link>
  );

  if (props.orcid) {
    return loggedInButton;
  }

  return (
    <OrcidButton
      className={props.className || 'button'}
      pendingElement={loggedInButton}
      redirectPath="/profile"
      title={props.title || 'Build your profile'}
    >
      <span className="icon">
        <img src={orcidIcon} alt=""/>
      </span>
      <span>{props.children}</span>
    </OrcidButton>
  );
};
// The following two functions are trivial (and forced to be correct through the types),
// but testing them requires inspecting the props of the connected component and mocking the store.
// Figuring that out is not worth it, so we just don't test this:
/* istanbul ignore next */
function mapStateToClaimButtonProps(state: AppState): ClaimButtonReduxProps {
  return { orcid: state.account.orcid };
}
const ClaimButton = connect<ClaimButtonReduxProps, {}, ClaimButtonOwnProps, AppState>(
  mapStateToClaimButtonProps,
)(BareClaimButton);
